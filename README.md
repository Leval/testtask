* virtualenv virtualenv/testtask
* source virtualenv/testtask/bin/activate
* pip install -r requirements.txt
* 
* python create_db.py

Search implemented with the help of Whoosh Search, so it will not index elements, which was added with the help of SQL script (it will index it only after "editing").