from wtforms import TextField, Form
from wtforms.validators import Required


class CreateAuthorForm(Form):
    name = TextField('name', validators=[Required()])