from books.models import Book
import flask_whooshalchemy
from run import app, db

books = db.Table('books_authors',
                 db.Column('book_id', db.Integer, db.ForeignKey('books.id')),
                 db.Column('author_id', db.Integer, db.ForeignKey('authors.id'))
)


class Author(db.Model):
    __tablename__ = 'authors'
    __searchable__ = ['name']

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    books = db.relationship(Book, secondary=books,
                            lazy='dynamic',
                            primaryjoin=(books.c.author_id == id),
                            secondaryjoin=(books.c.book_id == Book.id),
                            backref=db.backref('authors', lazy='dynamic'))

    def __repr__(self):
        return '<Author %r>' % (self.name)

    def is_author(self, book):
        return self.books.filter(books.c.book_id == book.id).count() > 0

    def add_book(self, book):
        if not self.is_author(book):
            self.books.append(book)
            return self

    def delete_book(self, book):
        if self.is_author(book):
            self.books.remove(book)
            return self


flask_whooshalchemy.whoosh_index(app, Author)

