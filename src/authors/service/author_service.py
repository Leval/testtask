from authors.models import Author
from run import db


class AuthorService(object):
    model_instance = Author

    def get_all(self):
        return self.model_instance.query.all()

    def get_by_id(self, id):
        return self.model_instance.query.get(id)

    def create_author(self, name):
        author = self.model_instance(name=name)
        db.session.add(author)
        db.session.commit()

    def delete_author(self, id):
        author = self.get_by_id(id)
        db.session.delete(author)
        db.session.commit()

    def edit_author(self, author, name):
        author.name = name
        db.session.add(author)
        db.session.commit()

    def author_whoosh_search(self, query):
        return self.model_instance.query.whoosh_search(query).all()
