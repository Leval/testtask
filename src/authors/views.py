from books.forms.forms import SearchForm
import flask
from authors.forms.forms import CreateAuthorForm
from flask import render_template, redirect, request, g
from authors.service.author_service import AuthorService
from authors import authors
from flask.ext.login import login_required

author_service = AuthorService()

@authors.before_request
def before_request():
    g.search_form = SearchForm()


@authors.route('/create_author', methods=['GET', 'POST'])
@login_required
def create_author():
    form = CreateAuthorForm(request.form)
    if request.method == 'POST' and form.validate():
        author_service.create_author(form.name.data)
        return flask.jsonify({'result': True})
    return render_template('create_author.html', title='Create Author', form=form, author=None)

@authors.route('/authors')
def authors_list():
    authors = author_service.get_all()
    return render_template('authors.html', authors=authors)

@authors.route('/authors/<author_id>')
def author(author_id):
    author = author_service.get_by_id(author_id)
    return render_template('author.html', author=author)

@authors.route('/delete/author/<author_id>')
@login_required
def delete_author(author_id):
    author_service.delete_author(author_id)
    return redirect('/authors')

@authors.route('/edit_author', methods=['GET', 'POST'])
@login_required
def edit_author():
    form = CreateAuthorForm(request.form)
    author = None
    if request.method == 'GET':
        author = author_service.get_by_id(request.args.get('author'))
        form = CreateAuthorForm(obj=author)
    if request.method == 'POST' and form.validate():
        author = author_service.get_by_id(request.form.get('author'))
        author_service.edit_author(author, form.name.data)
        return flask.jsonify({'result': True})
    return render_template('create_author.html', title='Create Author', form=form, author=author)