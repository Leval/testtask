from flask import Blueprint

books = Blueprint('books', __name__, template_folder='templates', static_folder='static', static_url_path='/static/books')
import views