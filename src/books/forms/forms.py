from wtforms import TextField, Form
from wtforms.validators import Required


class CreateBookForm(Form):
    title = TextField('title', validators = [Required()])


class AddAuthorForm(Form):
    author_id = TextField('author_id', validators = [Required()])


class SearchForm(Form):
    search = TextField('search', validators = [Required()])