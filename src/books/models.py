import flask_whooshalchemy
from run import app, db


class Book(db.Model):
    __tablename__ = 'books'
    __searchable__ = ['title']

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256))

    def __repr__(self):
        return '<Book title %r>' % (self.title)


flask_whooshalchemy.whoosh_index(app, Book)