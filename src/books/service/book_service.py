from books.models import Book
from run import db


class BookService(object):
    model_instance = Book

    def get_all(self):
        return self.model_instance.query.all()

    def get_by_id(self, id):
        return self.model_instance.query.get(id)

    def create_book(self, title):
        book = self.model_instance(title=title)
        db.session.add(book)
        db.session.commit()

    def delete_book(self, id):
        book = self.get_by_id(id)
        db.session.delete(book)
        db.session.commit()

    def edit_book(self, book, title):
        book.title = title
        db.session.add(book)
        db.session.commit()

    def book_whoosh_search(self, query):
        return self.model_instance.query.whoosh_search(query).all()
