from books.forms.forms import CreateBookForm, AddAuthorForm, SearchForm
from books.service.book_service import BookService
from authors.service.author_service import AuthorService
from flask import request, redirect, render_template, g
import flask
from books import books
from flask.ext.login import login_required


author_service = AuthorService()
book_service = BookService()

@books.route('/create_book', methods=['GET', 'POST'])
@login_required
def create_book():
    form = CreateBookForm(request.form)
    if request.method == 'POST' and form.validate():
        book_service.create_book(form.title.data)
        return flask.jsonify({'result': True})
    return render_template('create_book.html', title='Create Book', form=form, book=None)

@books.route('/books')
def books_list():
    books = book_service.get_all()
    return render_template('books.html', books=books)

@books.route('/books/<book_id>')
def book(book_id):
    book = book_service.get_by_id(book_id)
    return render_template('book.html', book=book)

@books.route('/delete/book/<book_id>')
@login_required
def delete_book(book_id):
    book_service.delete_book(book_id)
    return redirect('/books')

@books.route('/edit_book', methods=['GET', 'POST'])
@login_required
def edit_book():
    form = CreateBookForm(request.form)
    book = None
    if request.method == 'GET':
        book = book_service.get_by_id(request.args.get('book'))
        form = CreateBookForm(obj=book)
    if request.method == 'POST' and form.validate():
        book = book_service.get_by_id(request.form.get('book'))
        book_service.edit_book(book, form.title.data)
        return flask.jsonify({'result': True})
    return render_template('create_book.html', title='Create Book', form=form, book=book)

@books.route('/book/author/add', methods=['GET', 'POST'])
@login_required
def add_author_book():
    form = AddAuthorForm(request.form)
    book = None
    authors = author_service.get_all()
    if request.method == 'GET':
        book = book_service.get_by_id(request.args.get('book'))
    if request.method == 'POST' and form.validate():
        book = book_service.get_by_id(request.form.get('book'))
        author = author_service.get_by_id(form.author_id.data)
        author.add_book(book)
        return flask.jsonify({'result': True})
    return render_template('add_author.html', form=form, book=book, authors=authors)

@books.route('/book/author/delete')
@login_required
def delete_author_book():
    book = book_service.get_by_id(request.args.get('book'))
    author = author_service.get_by_id(request.args.get('author'))
    author.delete_book(book)
    return flask.jsonify({'result': True})

@books.before_request
def before_request():
    g.search_form = SearchForm()

@books.route('/search', methods=['GET', 'POST'])
def search():
    form = SearchForm(request.form)
    if not form.validate() and request.method != 'POST':
        return redirect('/')
    query = form.search.data
    results = book_service.book_whoosh_search(query)
    authors = author_service.author_whoosh_search(query)
    for author in authors:
        results.extend(author.books.all())

    return render_template('books.html', books=set(results))