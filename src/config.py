import os

CSRF_ENABLED = True
SECRET_KEY = 'secret_key'

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'library.db')
WHOOSH_BASE = os.path.join(basedir, 'whoosh_index')