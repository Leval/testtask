drop table if exists books;
drop table if exists authors;
drop table if exists books_authors;
drop table if exists users;

create table books (
  id INTEGER NOT NULL,
  title VARCHAR(256),
  PRIMARY KEY (id)
);

create table authors (
  id INTEGER NOT NULL,
  name VARCHAR(64),
  PRIMARY KEY (id)
);

create table books_authors (
  book_id INTEGER,
  author_id INTEGER,
  FOREIGN KEY(book_id) REFERENCES books (id),
  FOREIGN KEY(author_id) REFERENCES authors (id)
);

create table users (
  id INTEGER NOT NULL,
  username VARCHAR(64),
  password VARCHAR(64),
  role INTEGER,
  PRIMARY KEY (id)
);

insert into users (username, password, role) values ('admin', 'admin', 0);
insert into users (username, password, role) values ('editor', 'editor', 1);
insert into users (username, password, role) values ('user', 'user', 2);

insert into books (title) values ('first book');
insert into books (title) values ('second book');
insert into books (title) values ('third book');
insert into books (title) values ('fourth book');
insert into books (title) values ('fifth book');

insert into authors (name) values ('Dave');
insert into authors (name) values ('David Guetta');
insert into authors (name) values ('Simon');

insert into books_authors (book_id, author_id) values (1, 1);
insert into books_authors (book_id, author_id) values (1, 3);
insert into books_authors (book_id, author_id) values (2, 2);
insert into books_authors (book_id, author_id) values (3, 2);
insert into books_authors (book_id, author_id) values (3, 3);
insert into books_authors (book_id, author_id) values (4, 1);
insert into books_authors (book_id, author_id) values (4, 2);
insert into books_authors (book_id, author_id) values (5, 3);