from wtforms import TextField, Form, PasswordField
from wtforms.validators import Required, Length


class UserForm(Form):
    username = TextField('username', validators=[Required(), Length(min=4, max=25)])
    password = PasswordField('password', validators=[Required(), Length(min=4, max=25)])