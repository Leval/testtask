from run import db
from users.models import User


class UserService(object):
    model_instance = User

    def get_all(self):
        return self.model_instance.query.all()

    def get_by_id(self, id):
        return self.model_instance.query.get(id)

    def get_by_username_password(self, username, password):
        return self.model_instance.query.filter_by(username=username, password=password).first()

    def create_user(self, username, password):
        user = self.model_instance(username=username, password=password)
        db.session.add(user)
        db.session.commit()
        return user