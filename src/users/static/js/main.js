Library = window.Library || {};
Library.data = Library.data || {};


Library.data.main = {

    add_author_url: '',
    edit_author_url: '',
    add_book_url: '',
    edit_book_url: '',
    add_author_book_url: '',
    delete_author_book_url: '',

    init: function () {
        this.initCreateAuthor();
        this.initEditAuthor();
        this.initCreateBook();
        this.initEditBook();
        this.initAddAuthorBook();
        this.initDeleteAuthorBook();
    },

    initCreateAuthor: function () {
        var self = this;
        $('.add-author-button').on('click', function () {
            $.ajax({
                url: self.add_author_url,
                success: function (data) {
                    $(data).insertAfter('.add-author-button');
                    $('.add-author-button').hide();
                    self.initProcessCreateAuthor();

                    $('.cancel-add-author').on('click', function (e) {
                        e.preventDefault();
                        $('.create-author-form').remove();
                        $('.add-author-button').show();
                    });
                }
            })
        });
    },

    initProcessCreateAuthor: function () {
        var self = this;
        $('.save-add-author').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: self.add_author_url,
                data: $('.create-author-form').serializeArray(),
                type: 'post',
                success: function (data) {
                    if (data.result) {
                        location.pathname = '/authors';
                    } else {
                        $('.create-author-form').remove();
                        $(data).insertAfter('.add-author-button');
                        $('.add-author-button').hide();
                        self.initProcessCreateAuthor();
                    }
                }
            })
        });
    },

    initEditAuthor: function () {
        var self = this;
        $('.edit-author-button').on('click', function () {
            var author = $(this).data('author');
            $.ajax({
                url: self.edit_author_url,
                data: {author: author},
                success: function (data) {
                    $(data).insertAfter('.title-block');
                    $('.edit-author-button').hide();
                    self.initEditCreateAuthor();

                    $('.cancel-add-author').on('click', function (e) {
                        e.preventDefault();
                        $('.create-author-form').remove();
                        $('.edit-author-button').show();
                    });
                }
            })
        });
    },

    initEditCreateAuthor: function () {
        var self = this;
        $('.save-add-author').on('click', function (e) {
            e.preventDefault();
            var author = $(this).data('author');
            var data = $('.create-author-form').serializeArray();
            data.push({name: 'author', value: author});
            $.ajax({
                url: self.edit_author_url,
                data: data,
                type: 'post',
                success: function (data) {
                    if (data.result) {
                        location.reload();
                    } else {
                        $('.create-author-form').remove();
                        $(data).insertAfter('.title-block');
                        $('.edit-author-button').hide();
                        self.initEditCreateAuthor();
                    }
                }
            })
        });
    },

    initCreateBook: function () {
        var self = this;
        $('.add-book-button').on('click', function () {
            $.ajax({
                url: self.add_book_url,
                success: function (data) {
                    $(data).insertAfter('.add-book-button');
                    $('.add-book-button').hide();
                    self.initProcessCreateBook();

                    $('.cancel-add-book').on('click', function (e) {
                        e.preventDefault();
                        $('.create-book-form').remove();
                        $('.add-book-button').show();
                    });
                }
            })
        });
    },

    initProcessCreateBook: function () {
        var self = this;
        $('.save-add-book').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: self.add_book_url,
                data: $('.create-book-form').serializeArray(),
                type: 'post',
                success: function (data) {
                    if (data.result) {
                        location.pathname = '/books';
                    } else {
                        $('.create-book-form').remove();
                        $(data).insertAfter('.add-book-button');
                        $('.add-book-button').hide();
                        self.initProcessCreateBook();
                    }
                }
            })
        });
    },

    initEditBook: function () {
        var self = this;
        $('.edit-book-button').on('click', function () {
            var book = $(this).data('book');
            $('.delete-book-author-button').show();
            $.ajax({
                url: self.edit_book_url,
                data: {book: book},
                success: function (data) {
                    $(data).insertAfter('.title-block');
                    $('.edit-book-button').hide();
                    self.initEditCreateBook();

                    $('.cancel-add-book').on('click', function (e) {
                        e.preventDefault();
                        $('.delete-book-author-button').hide();
                        $('.create-book-form').remove();
                        $('.edit-book-button').show();
                    });
                }
            })
        });
    },

    initEditCreateBook: function () {
        var self = this;
        $('.save-add-book').on('click', function (e) {
            e.preventDefault();
            var book = $(this).data('book');
            var data = $('.create-book-form').serializeArray();
            data.push({name: 'book', value: book});
            $.ajax({
                url: self.edit_book_url,
                data: data,
                type: 'post',
                success: function (data) {
                    if (data.result) {
                        location.reload();
                    } else {
                        $('.create-book-form').remove();
                        $(data).insertAfter('.title-block');
                        $('.edit-book-button').hide();
                        self.initEditCreateBook();
                    }
                }
            })
        });
    },

    initAddAuthorBook: function () {
        var self = this;
        $('.add-book-author-button').on('click', function () {
            var book = $(this).data('book');
            $.ajax({
                url: self.add_author_book_url,
                data: {book: book},
                success: function (data) {
                    $(data).insertBefore('.add-book-author-button');
                    $('.add-book-author-button').hide();
                    self.initProcessAddAuthorBook();

                    $('.cancel-add-book-author').on('click', function (e) {
                        e.preventDefault();
                        $('.add-book-author-form').remove();
                        $('.add-book-author-button').show();
                    });
                }
            })
        });
    },

    initProcessAddAuthorBook: function () {
        var self = this;
        $('.save-add-book-author').on('click', function (e) {
            e.preventDefault();
            var book = $(this).data('book');
            var data = $('.add-book-author-form').serializeArray();
            data.push({name: 'book', value: book});

            $.ajax({
                url: self.add_author_book_url,
                data: data,
                type: 'post',
                success: function (data) {
                    if (data.result) {
                        location.reload();
                    } else {
                        $('.add-book-author-form').remove();
                        $(data).insertBefore('.add-book-author-button');
                        $('.add-book-author-button').hide();
                        self.initProcessAddAuthorBook();
                    }
                }
            })
        });
    },

    initDeleteAuthorBook: function () {
        var self = this;
        $('.delete-book-author-button').on('click', function () {
            var book = $(this).data('book');
            var author = $(this).data('author');
            $.ajax({
                url: self.delete_author_book_url,
                data: {book: book, author: author},
                success: function (data) {
                    if (data.result) {
                        $('.author-name-block[data-author="' + author + '"]').remove();
                    }
                }
            });
        });
    }

};

$(function () {
    Library.data.main.init();
});