from books.forms.forms import SearchForm
from flask import request, render_template, g
from flask.ext.login import login_required, logout_user, redirect, login_user
from users import users
from users.forms.forms import UserForm
from users import models
from users.service.user_service import UserService

user_service = UserService()

@users.before_request
def before_request():
    g.search_form = SearchForm()

@users.route('/')
def index():
    form = UserForm()
    form_login = UserForm()
    users = user_service.get_all()
    return render_template('index.html', form=form, form_login=form_login, users=users)

@users.route('/login_user', methods=['GET', 'POST'])
def login():
    form = UserForm()
    form_login = UserForm(request.form)
    if request.method == 'POST' and form_login.validate():
        user = user_service.get_by_username_password(form_login.username.data, form_login.password.data)
        if user:
            login_user(user)
            return redirect('/authors')
        else:
            return redirect('/')
    return render_template('index.html', title='Create User', form_login=form_login, form=form)

@users.route('/create_user', methods=['GET', 'POST'])
def create_user():
    form = UserForm(request.form)
    form_login = UserForm()
    if request.method == 'POST' and form.validate():
        user = user_service.create_user(form.username.data, form.password.data)
        if user:
            login_user(user)
        return redirect('/')
    return render_template('index.html', title='Create User', form=form, form_login=form_login)

@users.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect('/')